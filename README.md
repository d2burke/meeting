# YO for Meeting Reminders #

MEETING? is a simple app to help you remind coworkers that there is a meeting to go to.  The app lets users follow other coworkers and send them a **push notification** by tapping their username.

MEETING? also features a custom **Today widget** which displays a button for each of the user's top 8 coworkers which can be tapped to send them a notification without having to open the app.

MEETING? is built on the **Parse** SaaS platform