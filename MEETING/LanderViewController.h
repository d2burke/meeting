//
//  LanderViewController.h
//  MTG
//
//  Created by Daniel.Burke on 10/31/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanderViewController : UIViewController
<
UINavigationControllerDelegate
>

@property (nonatomic) CGFloat viewWidth;
@property (nonatomic) CGFloat viewHeight;

@property (strong, nonatomic) UIButton *signUpButton;
@property (strong, nonatomic) UIButton *logInButton;

@end
