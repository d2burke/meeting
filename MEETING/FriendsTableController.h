//
//  FriendsTableController.h
//  MTG
//
//  Created by Daniel.Burke on 10/31/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FriendCell.h"

@protocol FriendsTableControllerDelegate <NSObject>

- (void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath;

@end

@interface FriendsTableController : NSObject
<
UITableViewDelegate,
UITableViewDataSource,
UIScrollViewDelegate,
UITextFieldDelegate,
FriendCellDelegate
>

@property (strong, nonatomic) id <FriendsTableControllerDelegate> delegate;
@property (nonatomic) CGRect tableFrame;
@property (nonatomic) CGFloat lastContentOffsetY;
@property (strong, nonatomic) NSMutableArray *friends;

@property (strong, nonatomic) UIButton *addFriendButton;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UITextField *usernameTextField;

- (instancetype)initWithFrame:(CGRect)frame;

@end
