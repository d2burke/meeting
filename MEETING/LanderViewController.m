//
//  LanderViewController.m
//  MTG
//
//  Created by Daniel.Burke on 10/31/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "LanderViewController.h"
#import "LoginSigninViewController.h"
#import "FriendsViewController.h"
#import "TransitionLanderToForm.h"

@interface LanderViewController ()

@end

@implementation LanderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = BLUE;
    _viewWidth = self.view.frame.size.width;
    _viewHeight = self.view.frame.size.height;
    
    [self initView];
}

- (void)viewWillAppear:(BOOL)animated{
    
    PFUser *currentUser = [PFUser currentUser];
    if (currentUser) {
        FriendsViewController *friendsViewController = [[FriendsViewController alloc] init];
        friendsViewController.view.backgroundColor = BLUE;
        [self.navigationController pushViewController:friendsViewController animated:YES];
    } else {
        // show the signup or login screen
    }
    
    self.navigationController.delegate = self;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

#pragma mark - UINavigationController Delegate Methods
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
    // Check if we're transitioning from this view controller to a DSLSecondViewController
    if (fromVC == self && [toVC isKindOfClass:[LoginSigninViewController class]]) {
        return [[TransitionLanderToForm alloc] init];
    }
    else {
        return nil;
    }
}

- (void)viewForm:(UIButton*)button{
    LoginSigninViewController *formViewController = [[LoginSigninViewController alloc] init];
    formViewController.view.backgroundColor = BLUE;
    formViewController.formType = (int)button.tag;
    [self.navigationController pushViewController:formViewController animated:YES];
}

- (void)initView{
    _signUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _signUpButton.tag = 0;
    _signUpButton.frame = CGRectMake(0, _viewHeight/2, _viewWidth, 100);
    _signUpButton.titleLabel.font = [UIFont boldSystemFontOfSize:30.f];
    _signUpButton.backgroundColor = [BLUE darkerColor];
    [_signUpButton setTitle:@"SIGN UP" forState:UIControlStateNormal];
    [_signUpButton setTitleColor:WHITE forState:UIControlStateNormal];
    [_signUpButton addTarget:self  action:@selector(viewForm:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_signUpButton];
    
    _logInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _logInButton.tag = 1;
    _logInButton.frame = CGRectMake(0, _viewHeight/2-100, _viewWidth, 100);
    _logInButton.titleLabel.font = [UIFont boldSystemFontOfSize:30.f];
    _logInButton.backgroundColor = [[BLUE darkerColor] darkerColor];
    [_logInButton setTitle:@"LOG IN" forState:UIControlStateNormal];
    [_logInButton setTitleColor:WHITE forState:UIControlStateNormal];
    [_logInButton addTarget:self  action:@selector(viewForm:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_logInButton];
}

@end
