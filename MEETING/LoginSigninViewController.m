//
//  LoginSigninViewController.m
//  MTG
//
//  Created by Daniel.Burke on 10/31/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "LoginSigninViewController.h"
#import "FriendsViewController.h"
#import "LanderViewController.h"
#import "TransitionFormToLander.h"

@interface LoginSigninViewController ()

@end

@implementation LoginSigninViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _viewWidth = self.view.frame.size.width;
    _viewHeight = self.view.frame.size.height;
    
    [self initView];
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.delegate = self;
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    NSString *submitText = (_formType) ? @"LOG IN" : @"SIGN UP";
    UIColor *submitColor = (_formType) ? [[BLUE darkerColor] darkerColor] : [BLUE darkerColor];
    [_submitButton setTitle:submitText forState:UIControlStateNormal];
    _submitButton.backgroundColor = submitColor;
}


#pragma mark - UINavigationController Delegate Methods
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
    // Check if we're transitioning from this view controller to a DSLSecondViewController
    if (fromVC == self && [toVC isKindOfClass:[LanderViewController class]]) {
        return [[TransitionFormToLander alloc] init];
    }
    else {
        return nil;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound) {
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == _usernameTextField){
        [_passwordTextField becomeFirstResponder];
    }
    else{
        [self submitForm];
    }
    return YES;
}

- (void)submitForm{
    if(_formType){
        NSLog(@"Log In");
        [PFUser logInWithUsernameInBackground:_usernameTextField.text password:_passwordTextField.text
            block:^(PFUser *user, NSError *error) {
                if (user) {
                    FriendsViewController *friendsViewController = [[FriendsViewController alloc] init];
                    friendsViewController.view.backgroundColor = BLUE;
                    [self.navigationController pushViewController:friendsViewController animated:YES];
                } else {
                    [self.view endEditing:YES];
                    NSString *submitText = _submitButton.titleLabel.text;
                    NSString *errorString = [error userInfo][@"error"];
                    [_submitButton setTitle:errorString forState:UIControlStateNormal];
                    
                    [UIView animateWithDuration:0.24 delay:1 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        _submitButton.alpha = 0;
                    } completion:^(BOOL finished) {
                        [_submitButton setTitle:submitText forState:UIControlStateNormal];
                        _submitButton.alpha = 1;
                    }];
                }
            }];
    }
    else{
        
        PFUser *user = [PFUser user];
        user.username = _usernameTextField.text;
        user.password = _passwordTextField.text;
        
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                [currentInstallation addUniqueObject:_usernameTextField.text forKey:@"channels"];
                [currentInstallation saveInBackground];
                
                FriendsViewController *friendsViewController = [[FriendsViewController alloc] init];
                friendsViewController.view.backgroundColor = BLUE;
                [self.navigationController pushViewController:friendsViewController animated:YES];
            } else {
                NSString *submitText = _submitButton.titleLabel.text;
                NSString *errorString = [error userInfo][@"error"];
                NSString *message = [NSString stringWithFormat:@"username %@ already taken", user.username];

                if([errorString isEqualToString:message]){
                    errorString = @"username taken";
                }
                
                [_submitButton setTitle:errorString forState:UIControlStateNormal];
                
                [UIView animateWithDuration:0.24 delay:1 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    _submitButton.alpha = 0;
                } completion:^(BOOL finished) {
                    [_submitButton setTitle:submitText forState:UIControlStateNormal];
                    _submitButton.alpha = 1;
                }];
            }
        }];
    }
}

- (void)goBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initView{
    _usernameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, 100)];
    _usernameTextField.placeholder = @"USERNAME";
    _usernameTextField.textColor = WHITE;
    _usernameTextField.font = [UIFont boldSystemFontOfSize:30.f];
    _usernameTextField.textAlignment = NSTextAlignmentCenter;
    _usernameTextField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    _usernameTextField.delegate = self;
    [self.view addSubview:_usernameTextField];
    
    _passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 100, _viewWidth, 100)];
    _passwordTextField.placeholder = @"PASSWORD";
    _passwordTextField.textColor = WHITE;
    _passwordTextField.font = [UIFont boldSystemFontOfSize:30.f];
    _passwordTextField.backgroundColor = [BLUE lighterColor];
    _passwordTextField.textAlignment = NSTextAlignmentCenter;
    _passwordTextField.secureTextEntry = YES;
    _passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    _passwordTextField.delegate = self;
    _passwordTextField.returnKeyType = UIReturnKeyGo;
    [self.view addSubview:_passwordTextField];
    
    _submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _submitButton.frame = CGRectMake(0, 200, _viewWidth, 100);
    _submitButton.titleLabel.font = [UIFont boldSystemFontOfSize:30.f];
    _submitButton.backgroundColor = [BLUE darkerColor];
    [_submitButton setTitle:@"LOG IN" forState:UIControlStateNormal];
    [_submitButton setTitleColor:WHITE forState:UIControlStateNormal];
    [_submitButton addTarget:self action:@selector(submitForm) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_submitButton];
    
    _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _backButton.frame = CGRectMake(0, 300, _viewWidth, 100);
    _backButton.titleLabel.font = [UIFont boldSystemFontOfSize:30.f];
    _backButton.backgroundColor = [[[BLUE darkerColor] darkerColor] darkerColor];
    [_backButton setTitle:@"GO BACK" forState:UIControlStateNormal];
    [_backButton setTitleColor:WHITE forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_backButton];
}

@end
