//
//  TransitionFormToLander.h
//  MEETING
//
//  Created by Daniel.Burke on 11/1/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransitionFormToLander : NSObject <UIViewControllerAnimatedTransitioning>

@end
