//
//  TransitionFormToLander.m
//  MEETING
//
//  Created by Daniel.Burke on 11/1/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "TransitionFormToLander.h"
#import "LanderViewController.h"
#import "LoginSigninViewController.h"

@implementation TransitionFormToLander

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    LoginSigninViewController *fromViewController = (LoginSigninViewController*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    LanderViewController *toViewController = (LanderViewController*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *containerView = [transitionContext containerView];
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    
    UIButton *button = fromViewController.submitButton;
    UIView *buttonSnap = [button snapshotViewAfterScreenUpdates:NO];
    CGFloat buttonYPosition = (fromViewController.formType) ? toViewController.viewHeight/2-100 : toViewController.viewHeight/2;
    buttonSnap.frame = [containerView convertRect:button.frame fromView:button.superview];
    fromViewController.view.hidden = YES;
    
    toViewController.view.alpha = 0;
    [containerView addSubview:toViewController.view];
    [containerView addSubview:buttonSnap];
    
    [UIView animateWithDuration:duration delay:0 usingSpringWithDamping:0.75 initialSpringVelocity:0.25 options:UIViewAnimationOptionCurveEaseOut animations:^{
        //Move the card to the spot where the listing images will be
        buttonSnap.frame = CGRectMake(0, buttonYPosition, fromViewController.viewWidth, 100);
        toViewController.view.alpha = 1;
    } completion:^(BOOL finished) {
        fromViewController.view.hidden = NO;
        [buttonSnap removeFromSuperview];
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
    }];
}

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    return 0.34;
}

@end
