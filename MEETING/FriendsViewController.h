//
//  FriendsViewController.h
//  MTG
//
//  Created by Daniel.Burke on 10/31/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsTableController.h"

@interface FriendsViewController : UIViewController
<
UINavigationControllerDelegate,
FriendsTableControllerDelegate  
>

@property (nonatomic) CGFloat viewWidth;
@property (nonatomic) CGFloat viewHeight;
@property (strong, nonatomic) PFUser *currentUser;
@property (strong, nonatomic) FriendsTableController *tableController;

@property (strong, nonatomic) UILabel *alertLabel;
@property (strong, nonatomic) UITableView *friendsTableView;

@end
