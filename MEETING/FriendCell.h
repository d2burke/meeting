//
//  FriendCell.h
//  MTG
//
//  Created by Daniel.Burke on 10/31/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    SwipeDirectionRight,
    SwipeDirectionLeft,
    SwipeDirectionDefault
} SwipeDirection;

@protocol FriendCellDelegate <NSObject>

@property (nonatomic) BOOL panning;
- (void)setTableScrollEnabed:(BOOL)enabled;
@end

@interface FriendCell : UITableViewCell <UIGestureRecognizerDelegate>

@property (strong, nonatomic) id <FriendCellDelegate> delegate;
@property (copy, nonatomic) UILabel *usernameLabel;

@end
