//
//  Colors.h
//  MTG
//
//  Created by Daniel.Burke on 10/31/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define WHITE [UIColor whiteColor]
#define CLEAR [UIColor clearColor]
#define BLUE UIColorFromRGB(0x65bcff)
#define FACEBOOK_BLUE UIColorFromRGB(0x4667AC)
#define TWITTER_BLUE UIColorFromRGB(0x22B2EF)
#define GOOGLE_RED UIColorFromRGB(0xDA4936)
#define PINTEREST_RED UIColorFromRGB(0xCB2028)
#define EMAIL_BLUE UIColorFromRGB(0x46b5ef)
#define MESSAGES_GREEN UIColorFromRGB(0x4fec43)

@interface Colors : NSObject

@end
