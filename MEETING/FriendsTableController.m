//
//  FriendsTableController.m
//  MTG
//
//  Created by Daniel.Burke on 10/31/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "FriendsTableController.h"
#import "FriendCell.h"
#import "UIColor+LightAndDark.h"

@implementation FriendsTableController

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super init];
    if(self){
        _tableFrame = frame;
        _friends = [[NSMutableArray alloc] init];
        _lastContentOffsetY = 0;
    }
    return self;
}

- (void)showAddFriendForm{
    _addFriendButton.hidden = YES;
    _usernameTextField.hidden = NO;
    [_usernameTextField becomeFirstResponder];
}

-(void)setTableScrollEnabed:(BOOL)enabled{
    [_tableView setScrollEnabled:enabled];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" equalTo:_usernameTextField.text];
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!object) {
            _usernameTextField.text = @"No Such User";
            [UIView animateWithDuration:0.34 delay:2 options:UIViewAnimationOptionCurveEaseIn animations:^{
                _usernameTextField.alpha = 0;
            } completion:^(BOOL finished) {
                _addFriendButton.hidden = NO;
                _usernameTextField.hidden = YES;
                _usernameTextField.alpha = 1;
            }];
        } else {
            PFUser *friend = (PFUser*)object;
            [_friends addObject:friend];
            [_usernameTextField resignFirstResponder];
            [_tableView reloadData];
            
            PFObject *newfriend = [PFObject objectWithClassName:@"Friend"];
            newfriend[@"User"] = [PFUser currentUser];
            newfriend[@"Friend"] = friend;
            [newfriend saveInBackground];
        }
    }];
    return YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //If scrolling up, bring back Chrome elements
    if (_lastContentOffsetY > scrollView.contentOffset.y){
        [_usernameTextField resignFirstResponder];
        [_tableView reloadData];
    }
    _lastContentOffsetY = scrollView.contentOffset.y;
}

#pragma mark - UITableView Delegate Methods

- (NSArray*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        PFUser *friendUser = (PFUser*)[_friends objectAtIndex:indexPath.row];
        [friendUser fetchIfNeeded];
        PFQuery *query = [PFQuery queryWithClassName:@"Friend"];
        [query whereKey:@"Friend" equalTo:friendUser];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            [object deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded && !error) {
                    [_friends removeObjectAtIndex:indexPath.row];
                    [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                } else {
                    NSLog(@"error: %@", error);
                }
            }];
        }];
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    
    return @[deleteAction];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PFUser *currentUser = [PFUser currentUser];
    FriendCell *cell = (FriendCell*)[_tableView cellForRowAtIndexPath:indexPath];
    cell.usernameLabel.text = @"SENDING MSG...";
    PFUser *friend = (PFUser*)[_friends objectAtIndex:indexPath.row];
    NSString *message = [NSString stringWithFormat:@"%@: MEETING?", currentUser.username];
    
    // Send a notification to all devices subscribed to the "Giants" channel.
    PFPush *push = [[PFPush alloc] init];
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          message,@"alert",
                          @"default",@"sound",
                          @"Increment", @"badge",
                          nil];
    [push setChannel:friend.username];
    [push setData:data];
    [push sendPushInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            cell.usernameLabel.text = @"MTG MSG SENT!";
            [UIView animateWithDuration:0.34 delay:2 options:UIViewAnimationOptionCurveEaseIn animations:^{
                cell.usernameLabel.alpha = 0;
            } completion:^(BOOL finished) {
                cell.usernameLabel.text = friend.username;
                cell.usernameLabel.alpha = 1;
            }];
        }
        else{
            
        }
    }];
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableFrame.size.width, 100)];
    footerView.backgroundColor = [[[[[BLUE darkerColor] darkerColor] darkerColor] darkerColor] darkerColor];
    
    _addFriendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _addFriendButton.frame = CGRectMake(0, 0, _tableFrame.size.width, 100);
    _addFriendButton.titleLabel.font = [UIFont boldSystemFontOfSize:30.f];
    _addFriendButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [_addFriendButton setTitle:@"+" forState:UIControlStateNormal];
    [_addFriendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_addFriendButton addTarget:self action:@selector(showAddFriendForm) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_addFriendButton];
    
    _usernameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, _tableFrame.size.width, 100)];
    _usernameTextField.placeholder = @"USERNAME";
    _usernameTextField.textColor = WHITE;
    _usernameTextField.font = [UIFont boldSystemFontOfSize:30.f];
    _usernameTextField.textAlignment = NSTextAlignmentCenter;
    _usernameTextField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    _usernameTextField.delegate = self;
    _usernameTextField.hidden = YES;
    _usernameTextField.returnKeyType = UIReturnKeyGo;
    [footerView addSubview:_usernameTextField];
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.00001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //Later, calc height based on text in comment
    return  100;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_friends count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"cell";
    FriendCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[FriendCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    PFUser *friend = (PFUser*)[_friends objectAtIndex:indexPath.row];
    cell.usernameLabel.text = friend.username;
    cell.delegate = self;
    UIColor *bgColor;
    switch (indexPath.row%5) {
        case 0:
            bgColor = BLUE;
            break;
        case 1:
            bgColor = [BLUE darkerColor];
            break;
        case 2:
            bgColor = [[BLUE darkerColor] darkerColor];
            break;
        case 3:
            bgColor = [[[BLUE darkerColor] darkerColor] darkerColor];
            break;
        case 4:
            bgColor = [[[[BLUE darkerColor] darkerColor] darkerColor] darkerColor];
            break;
            
        default:
            break;
    }
    cell.usernameLabel.backgroundColor = bgColor;
    
    return cell;
}
@end
