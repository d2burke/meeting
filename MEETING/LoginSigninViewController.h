//
//  LoginSigninViewController.h
//  MTG
//
//  Created by Daniel.Burke on 10/31/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginSigninViewController : UIViewController
<
UINavigationControllerDelegate,
UITextFieldDelegate
>

@property (nonatomic) int formType;
@property (nonatomic) CGFloat viewWidth;
@property (nonatomic) CGFloat viewHeight;

@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UIButton *submitButton;
@property (strong, nonatomic) UITextField *usernameTextField;
@property (strong, nonatomic) UITextField *passwordTextField;

@end
