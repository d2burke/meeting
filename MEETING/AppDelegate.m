//
//  AppDelegate.m
//  MEETING
//
//  Created by Daniel.Burke on 10/31/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "AppDelegate.h"
#import "LanderViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UIMutableUserNotificationAction *action = [[UIMutableUserNotificationAction alloc] init];
    action.identifier = @"ACTION_ID"; // The id passed when the user selects the action
    action.title = NSLocalizedString(@"Title",nil); // The title displayed for the action
    action.activationMode = UIUserNotificationActivationModeBackground; // Choose whether the application is launched in foreground when the action is clicked
    action.destructive = NO; // If YES, then the action is red
    action.authenticationRequired = NO;
    
    UIMutableUserNotificationCategory *category = [[UIMutableUserNotificationCategory alloc] init];
    category.identifier = @"CATEGORY_ID"; // Identifier passed in the payload
    [category setActions:@[action] forContext:UIUserNotificationActionContextDefault]; // The context determines the number of actions presented (see documentation)
    
    NSSet *categories = [NSSet setWithObjects:category, nil];
    NSUInteger types = UIUserNotificationTypeNone; // Add badge, sound, or alerts here
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:categories];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    
    [Parse setApplicationId:@"cLVdppLhlnJBY1UTSgZoZnPLTocOlWFLuJ2CAcP8"
                  clientKey:@"9ctOnmv6WzEFJk1Y8dPYs4msI8i7vk1H64wlnrnZ"];
    
    // Register for Push Notitications, if running iOS 8
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    } else {
        // Register for Push Notifications before iOS 8
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                         UIRemoteNotificationTypeAlert |
                                                         UIRemoteNotificationTypeSound)];
    }
    
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    LanderViewController *landerViewController = [[LanderViewController alloc] init];
    landerViewController.view.backgroundColor = BLUE;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:landerViewController];
    _window.rootViewController = navController;
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    UIViewController *vc = self.window.rootViewController;    
    UILabel *alertLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, -100, vc.view.frame.size.width, 100)];
    alertLabel.textColor = WHITE;
    alertLabel.font = [UIFont boldSystemFontOfSize:24.f];
    alertLabel.textAlignment = NSTextAlignmentCenter;
    alertLabel.backgroundColor = GOOGLE_RED;
    alertLabel.numberOfLines = 2;
    [vc.view addSubview:alertLabel];
    
    NSLog(@"Push: %@", [[userInfo objectForKey:@"aps"] objectForKey:@"alert"]);
    
    alertLabel.text = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    CGRect alertFrame = alertLabel.frame;
    alertFrame.origin.y = 0;
    [UIView animateWithDuration:0.3 animations:^{
        alertLabel.frame = alertFrame;
    } completion:^(BOOL finished) {
        CGRect alertFrame = alertLabel.frame;
        alertFrame.origin.y = -100;
        [UIView animateWithDuration:0.34 delay:2 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            alertLabel.frame = alertFrame;
        } completion:^(BOOL finished) {
            completionHandler(UIBackgroundFetchResultNewData);
        }];
    }];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Push w/o background: %@", userInfo);
    UIViewController *vc = self.window.rootViewController;
    UILabel *alertLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, -100, vc.view.frame.size.width, 100)];
    alertLabel.textColor = WHITE;
    alertLabel.font = [UIFont boldSystemFontOfSize:24.f];
    alertLabel.textAlignment = NSTextAlignmentCenter;
    alertLabel.backgroundColor = GOOGLE_RED;
    alertLabel.numberOfLines = 2;
    [vc.view addSubview:alertLabel];
    
    alertLabel.text = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    CGRect alertFrame = alertLabel.frame;
    alertFrame.origin.y = 0;
    [UIView animateWithDuration:0.3 animations:^{
        alertLabel.frame = alertFrame;
    } completion:^(BOOL finished) {
        CGRect alertFrame = alertLabel.frame;
        alertFrame.origin.y = -100;
        [UIView animateWithDuration:0.34 delay:2 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            alertLabel.frame = alertFrame;
        } completion:^(BOOL finished) {
        }];
    }];
    [PFPush handlePush:userInfo];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler{
    NSLog(@"Remote Note: %@", userInfo);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    if (currentInstallation.badge != 0) {
        currentInstallation.badge = 0;
        [currentInstallation saveEventually];
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.d2burke.MEETING" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MEETING" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MEETING.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
