//
//  FriendsViewController.m
//  MTG
//
//  Created by Daniel.Burke on 10/31/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "FriendsViewController.h"
#import "UIColor+LightAndDark.h"

@interface FriendsViewController ()

@end

@implementation FriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    _viewWidth = self.view.frame.size.width;
    _viewHeight = self.view.frame.size.height;
    _currentUser = [PFUser currentUser];
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.MEETING"];
    [sharedDefaults setObject:_currentUser.username forKey:@"Username"];
    [sharedDefaults synchronize];   // (!!) This is crucial.
    
    [self initView];
    
    if(_currentUser){
        PFInstallation *currentInstallation = [PFInstallation currentInstallation];
        [currentInstallation addUniqueObject:_currentUser.username forKey:@"channels"];
        [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if(!succeeded){
                NSLog(@"Error: %@", error);
            }
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.delegate = self;
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    if(_currentUser){
        PFQuery *query = [PFQuery queryWithClassName:@"Friend"];
        [query whereKey:@"User" equalTo:[PFUser currentUser]];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            for(PFObject *friend in objects){
                PFUser *user = (PFUser*)friend[@"Friend"];
                [user fetchIfNeeded];
                [_tableController.friends addObject:user];
            }
            [_tableController.tableView reloadData];
        }];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Keyboard Methods
-(void)keyboardWillShow:(NSNotification*)notification{
    UIEdgeInsets notesInset = _friendsTableView.contentInset;
    
    // keyboard frame is in window coordinates
    NSDictionary *userInfo = [notification userInfo];
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGFloat keyboardHeight = keyboardFrame.size.height;
    notesInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0);
    
    [UIView animateWithDuration:0.34 animations:^{
        _friendsTableView.contentInset = notesInset;
    }];
}

-(void)keyboardWillHide:(NSNotification*)notification{
    [UIView animateWithDuration:0.34 animations:^{
        _friendsTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - FriendsTableControllerDelegate Methods
- (void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)displayAlertLabelWithMessage:(NSString*)message{
    _alertLabel.text = message;
    CGRect alertFrame = _alertLabel.frame;
    alertFrame.origin.y = 0;
    [UIView animateWithDuration:0.3 animations:^{
        _alertLabel.frame = alertFrame;
    } completion:^(BOOL finished) {
        CGRect alertFrame = _alertLabel.frame;
        alertFrame.origin.y = -120;
        [UIView animateWithDuration:0.34 delay:2 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            _alertLabel.frame = alertFrame;
        } completion:^(BOOL finished) {
            //
        }];
    }];
}

- (void)logout{
    [PFUser logOut];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)initView{
    _tableController = [[FriendsTableController alloc] initWithFrame:self.view.frame];
    
    _friendsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, _viewHeight) style:UITableViewStyleGrouped];
    _friendsTableView.delegate = _tableController;
    _friendsTableView.dataSource = _tableController;
    _friendsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _friendsTableView.backgroundColor = BLUE;
    [self.view addSubview:_friendsTableView];
    
    _tableController.tableView = _friendsTableView;
    
    UIView *logoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, 100)];
    logoutView.backgroundColor = [BLUE darkerColor];
    
    UIButton *logOutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    logOutButton.frame = logoutView.frame;
    logOutButton.titleLabel.font = [UIFont boldSystemFontOfSize:30.f];
    PFUser *currentUser = [PFUser currentUser];
    NSString *logOutText = [NSString stringWithFormat:@"LOGOUT: %@", currentUser.username];
    [logOutButton setTitle:logOutText forState:UIControlStateNormal];
    [logOutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [logoutView addSubview:logOutButton];
    [logOutButton addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    _friendsTableView.tableFooterView = logoutView;
    
    _alertLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, -120, _viewWidth, 120)];
    _alertLabel.text = @"D2BURKE: MEETING?";
    _alertLabel.textColor = WHITE;
    _alertLabel.font = [UIFont boldSystemFontOfSize:30.f];
    _alertLabel.textAlignment = NSTextAlignmentCenter;
    _alertLabel.backgroundColor = GOOGLE_RED;
    [self.view addSubview:_alertLabel];
}

@end
