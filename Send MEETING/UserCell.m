//
//  UserCell.m
//  MEETING
//
//  Created by Daniel.Burke on 11/2/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "UserCell.h"

@implementation UserCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _userButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _userButton.frame = CGRectMake(0, 0, 80, 80);
        _userButton.layer.cornerRadius = 25.f;
        _userButton.clipsToBounds = YES;
        _userButton.titleLabel.font = [UIFont boldSystemFontOfSize:12.f];
        [_userButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview:_userButton];
    }
    return self;
}

- (void)layoutSubviews{
    _userButton.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

@end
