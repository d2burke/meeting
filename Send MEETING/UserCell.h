//
//  UserCell.h
//  MEETING
//
//  Created by Daniel.Burke on 11/2/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserCell : UICollectionViewCell

@property (copy, nonatomic) UIButton *userButton;

@end
