//
//  TodayViewController.m
//  Send MEETING
//
//  Created by Daniel.Burke on 11/2/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "TodayViewController.h"
#import "UserCell.h"
#import "Colors.h"
#import "UIColor+LightAndDark.h"
#import <NotificationCenter/NotificationCenter.h>

@interface TodayViewController () <NCWidgetProviding>

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userDefaultsDidChange:)
                                                 name:NSUserDefaultsDidChangeNotification
                                               object:nil];
    
    NSLog(@"Load");
    [self setPreferredContentSize:CGSizeMake(0, 160)];
    [Parse setApplicationId:@"cLVdppLhlnJBY1UTSgZoZnPLTocOlWFLuJ2CAcP8"
                  clientKey:@"9ctOnmv6WzEFJk1Y8dPYs4msI8i7vk1H64wlnrnZ"];
    
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    CGRect collectionFrame = CGRectMake(0, 0, self.view.frame.size.width, 160);
    _collectionView = [[UICollectionView alloc] initWithFrame:collectionFrame collectionViewLayout:layout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.directionalLockEnabled = YES;
    _collectionView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:_collectionView];
    
    _friends = [[NSMutableArray alloc] init];
    
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.MEETING"];
    NSString *username = [sharedDefaults objectForKey:@"Username"];
    if([sharedDefaults objectForKey:@"Username"]){
        PFQuery *query = [PFUser query];
        [query whereKey:@"username" equalTo:username];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (object) {
                _currentUser = (PFUser*)object;
                PFQuery *query = [PFQuery queryWithClassName:@"Friend"];
                [query whereKey:@"User" equalTo:_currentUser];
                [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    for(PFObject *friend in objects){
                        if([objects indexOfObject:friend] > 7){
                            break;
                        }
                        PFUser *user = (PFUser*)friend[@"Friend"];
                        [user fetchIfNeeded];
                        [_friends addObject:user];
                    }
                    [_collectionView reloadData];
                }];
            }
        }];
    }
}
- (void)userDefaultsDidChange:(NSNotification *)notification {
    [self updateNumberLabelText];
}

- (void)updateNumberLabelText {
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.MEETING"];
    NSLog(@"Current User: %@", [sharedDefaults objectForKey:@"Username"]);
}

- (void)tappedButton:(UIButton*)button{
    CGPoint buttonPosition = [button convertPoint:CGPointZero toView:_collectionView];
    NSIndexPath *indexPath = [_collectionView indexPathForItemAtPoint:buttonPosition];
    PFUser *friend = (PFUser*)[_friends objectAtIndex:indexPath.row];
    NSString *message = [NSString stringWithFormat:@"%@: MEETING?", _currentUser.username];
    
    // Send a notification to all devices subscribed to the "Giants" channel.
    PFPush *push = [[PFPush alloc] init];
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          message,@"alert",
                          @"default",@"sound",
                          @"Increment", @"badge",
                          nil];
    [push setChannel:friend.username];
    [push setData:data];
    [push sendPushInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            [button setTitle:@"SENT!" forState:UIControlStateNormal];
            [UIView animateWithDuration:0.34 delay:2 options:UIViewAnimationOptionCurveEaseIn animations:^{
                button.alpha = 0;
            } completion:^(BOOL finished) {
                [button setTitle:friend.username forState:UIControlStateNormal];
                button.alpha = 1;
            }];
        }
        else{
            
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

- (UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)defaultMarginInsets{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark UICollectionViewDelegate Methods
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Test");
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_friends count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    [_collectionView registerClass:[UserCell class] forCellWithReuseIdentifier:@"challenge"];
    
    UserCell *cell= (UserCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"challenge" forIndexPath:indexPath];
    [cell.userButton addTarget:self action:@selector(tappedButton:) forControlEvents:UIControlEventTouchUpInside];
    if([_friends count]){
        PFUser *friend = (PFUser*)[_friends objectAtIndex:indexPath.row];
        [cell.userButton setTitle:friend.username forState:UIControlStateNormal];
    }
    else{
        [cell.userButton setTitle:@"Username" forState:UIControlStateNormal];
    }
    
    
    UIColor *bgColor = BLUE;
    switch (indexPath.row) {
        case 0:
            bgColor = BLUE;
            break;
        case 1:
            bgColor = [BLUE colorWithAlphaComponent:0.9];
            break;
        case 2:
            bgColor = [BLUE colorWithAlphaComponent:0.8];
            break;
        case 3:
            bgColor = [BLUE colorWithAlphaComponent:0.7];
            break;
        case 4:
            bgColor = [BLUE colorWithAlphaComponent:0.6];
            break;
        case 5:
            bgColor = [BLUE colorWithAlphaComponent:0.5];
            break;
        case 6:
            bgColor = [BLUE colorWithAlphaComponent:0.4];
            break;
        case 7:
            bgColor = [BLUE colorWithAlphaComponent:0.3];
            break;
            
        default:
            break;
    }
    cell.backgroundColor = bgColor;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat buttonWidth = self.view.frame.size.width/4;
    return CGSizeMake(buttonWidth, 80);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}

@end
