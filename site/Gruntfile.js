module.exports = (function(grunt) {
    'use strict';
    grunt.initConfig({
        'uglify': {
            my_target: {
              files: {
                '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/js/site.min.js': 
                [
                    '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/js/src/site.js',
                ]
              }
            }
        },
        'cssmin': {
          combine: {
            files: {
                //Site CSS
                '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/css/meeting-min.css': 
                [   
                    '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/css/src/style.css',
                    '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/css/src/meeting.css',
                    '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/css/src/responsive.css'
                ]            }
          }
        },

        'ftp-deploy': {
          build: {
            auth: {
              host: 'meeting.d2.io',
              port: 21,
              authKey: 'key1'
            },
            src: '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site',
            dest: 'domains/meeting.d2.io/html',
            exclusions: [
                        '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/.htaccess',
                        '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/.htaccess',
                        '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/.DS_Store',
                        '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/node_modules',
                        '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/.git',
                        '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/.gitignore',
                        '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/.ftppass',
                        '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/package.json',
                        '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/Gruntfile.js',
                        '/Users/danielburke/Documents/Development/D2/iOS/MEETING/site/sftp-config.json'
            ]
          }
        }

    });

    grunt.loadNpmTasks('grunt-ftp-deploy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', [
        'uglify',
        'cssmin',
        'ftp-deploy'
    ]);

    grunt.registerTask('dev', [
        'uglify',
        'cssmin'
    ]);
});